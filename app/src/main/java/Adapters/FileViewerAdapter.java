package Adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.Time;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import Models.RecordingItem;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FileViewerAdapter extends RecyclerView.Adapter<FileViewerAdapter.FileViewerViewHolder> {

    Context context;
    ArrayList<RecordingItem> arrayList;
    LinearLayoutManager llm;

    public FileViewerAdapter(Context context, ArrayList<RecordingItem>arrayList, LinearLayoutManager llm){
        this.context = context;
        this.arrayList = arrayList;
        this.llm = llm;
    }

    @NonNull
    @Override
    public FileViewerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_view, viewGroup, false);
        return new FileViewerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FileViewerViewHolder holder, int i) {
        RecordingItem recordingItem = arrayList.get(i);

        long minutes = TimeUnit.MILLISECONDS.toMinutes(recordingItem.getLength());
        long seconds = TimeUnit.MILLISECONDS.toSeconds(recordingItem.getLength()) - TimeUnit.MINUTES.toSeconds(minutes);

        holder.vName.setText(recordingItem.getName());
        holder.vLenght.setText(String.format("%02d:%02d", minutes, seconds));

        holder.vTimeAdded.setText(DateUtils.formatDateTime(context, recordingItem.getTime_added(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_TIME
        | DateUtils.FORMAT_SHOW_YEAR
        ));

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class FileViewerViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.file_name_text) TextView vName;
        @BindView(R.id.file_length_text) TextView vLenght;
        @BindView(R.id.file_time_added) TextView vTimeAdded;
        @BindView(R.id.card_view) View card_view;

        public FileViewerViewHolder(@NonNull View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
