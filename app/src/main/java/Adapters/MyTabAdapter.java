package Adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import Fragments.FileViewerFragment;
import Fragments.RecordFragment;

public class MyTabAdapter extends FragmentPagerAdapter {
    String[] titles = {"Records", "Saved Recording"};

    public MyTabAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int i){
        switch (i)
        {
            case 0:
                return new RecordFragment();
                case 1:
                    return new FileViewerFragment();
        }
        return null;
    }

    @Override
    public int getCount(){
        return titles.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position){
        return titles[position];
    }
}
