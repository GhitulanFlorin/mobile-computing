package Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import Adapters.FileViewerAdapter;
import Models.RecordingItem;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FileViewerFragment extends Fragment {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    DBHelper dbHelper;
    private FileViewerAdapter fileViewerAdapter;

    ArrayList<RecordingItem> arrayListAudios;


    @Nullable
    @Override
    public View onCreateView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_file_viewer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@Nullable View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dbHelper = new DBHelper(GetContext());
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());

        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);
        recyclerView.setLayoutManager(llm);

        arrayListAudios = dbHelper.getAllAudios();

        if(arrayListAudios == null){
            Toast.makeText(getContext(), "No audio files", Toast.LENGTH_LONG).show();
        }
        else{
            fileViewerAdapter = new FileViewerAdapter(getActivity(), arrayListAudios, llm);
            recyclerView.setAdapter(fileViewerAdapter);
        }


    }
}
